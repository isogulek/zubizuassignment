/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import {
  StackNavigator,
} from 'react-navigation';
import CampaignList from './src/pages/campaignlist/CampaignList';
import CampaignDetail from './src/pages/campaigndetail/CampaignDetail';

const ZubizuAssignmentProject = StackNavigator({
  List: {screen: CampaignList},
  CampaignDetail: {screen: CampaignDetail}
},
{
  headerMode: 'float'
});

AppRegistry.registerComponent('ZubizuAssignmentProject', () => ZubizuAssignmentProject);
