import React from 'react';
import { View, Text, StyleSheet, Image, Dimensions, ScrollView, WebView, Alert } from 'react-native';

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#E7366B'
  },
  title: {
    margin: 8,
    fontSize: 16,
    fontWeight: 'bold',
    fontFamily: 'Futura',
  },
  detail: {
    margin: 8,
    fontSize: 15,
    fontFamily: 'Futura',
  }
});

class CampaignDetail extends React.Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    title: navigation.state.params.Name,
    fontFamily: 'Futura',
    headerTintColor: '#FFFFFF',
    headerStyle: styles.header
  });

  params = this.props.navigation.state.params;
  state = {
    imgWidth: 0,
    imgHeight: 0
  }

  componentDidMount() {

    Image.getSize(this.params.ImageUrl, (width, height) => {
      // calculate image width and height
      const screenWidth = Dimensions.get('window').width
      const scaleFactor = width / screenWidth
      const imageHeight = height / scaleFactor
      this.setState({imgWidth: screenWidth, imgHeight: imageHeight})
      //  numberOfLines={5} ellipsizeMode='tail'
    })
  }

  render() {
    const {imgWidth, imgHeight} = this.state
    const htmlStart = "<html><head><style>* {max-width: 100%;} body {font-family: Futura; margin: 8px;} img {margin:0px;}</style></head><body>";
    const htmlEnd = "</body></html>";

    //  This page should be rendered with a container ScrollView with a non-scrollable WebView, whose height should be calculated using Javascript.
    //  But i cannot find a proper way to do it. As i try it out, i saw all the ways buggy.
    //  Or image and title elements can be placed in HTML, with some additional start html:
    //  "<img src='" + this.params.ImageUrl + "' width=100%/><br/><br/>" + "<b>" + this.params.Name + "</b><br/><br/>" +

    return (
      <View style={{flex: 1}}>
        <Image
          style={{width: imgWidth, height: imgHeight}}
          source={{uri: this.params.ImageUrl}}
        />
        <Text style={styles.title}>
          {this.params.Name}
        </Text>
        <WebView
          bounces={false}
          automaticallyAdjustContentInsets={false}
          style={{flex:1}}
          source={{html: htmlStart + this.params.LongDescription + htmlEnd}} />
      </View>
    )
  }

}

export default CampaignDetail;
