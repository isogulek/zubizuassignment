import React from 'react';
import { View, Text, StyleSheet, Image, ListView, TouchableHighlight, ActivityIndicator, Alert } from 'react-native';
import {
  StackNavigator,
} from 'react-navigation';
import CampaignRow from './CampaignRow';
import CampaignDetail from '../campaigndetail/CampaignDetail';

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#E7366B'
  },
  centering: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  hud: {
    backgroundColor: '#000000DD',
    width: 64,
    height: 64,
    borderRadius: 8
  }
});

class CampaignList extends React.Component {

  static navigationOptions = {
    title: 'CAMPAIGNS',
    fontFamily: 'Futura',
    headerTintColor: '#FFFFFF',
    headerStyle: styles.header,
    headerBackTitle: 'Back' //  when navigated to detail, 'Back' will be displayed
  };

  state = {
    dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1._id !== r2._id}).cloneWithRows([]),
    isLoading: false
  };

  navigateToDetail(rowData) {
    const { navigate } = this.props.navigation;
    navigate('CampaignDetail', rowData);
  }

  getCampaigns() {
    this.setState({datasource: this.state.dataSource, isLoading: true});
    return fetch('http://nodejs-mongo-persistent2-zubizu-assignment.1d35.starter-us-east-1.openshiftapps.com/campaigns', {
        method: "GET",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      })
      .then((response) => response.json())
      .then((responseJson) => {
        //  setting state will re-render the view
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(responseJson),
          isLoading: false
        });
      })
      .catch((error) => {
        this.setState({
          dataSource: this.state.dataSource,
          isLoading: false
        });
        Alert.alert('Error', 'Network request failed.', [
              {text: 'Try Again', onPress: () => this.getCampaigns()},
            ]);
      });
  }

  componentDidMount() {
    this.getCampaigns();
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.centering}>
          <ActivityIndicator
            animating={true}
            style={styles.hud}
            size="large"
          />
        </View>);
    }else {
      return (<ListView
          enableEmptySections={true}
          dataSource={this.state.dataSource}
          renderSeparator={(sectionID, rowID, adjacentRowHighlighted) => this._renderSeparator(rowID)}
          renderRow={(rowData) => (
            <TouchableHighlight onPress={() => {this.navigateToDetail(rowData);}} underlayColor='#00000033'>
              <CampaignRow {...rowData} />
            </TouchableHighlight>
          )}
        />);
    }
  }

  _renderSeparator(rowID) {
    //  do not show separator for last row
    const rowCount = this.state.dataSource.getRowCount();
    return (<View style={{
          height: 1,
          backgroundColor: (rowID == rowCount - 1) ? '#FFFFFF00' : '#E7366B',
        }}
      />);
  }

}

export default CampaignList;
