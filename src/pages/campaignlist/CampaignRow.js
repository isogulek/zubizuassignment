import React from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  title: {
    margin: 8,
    fontFamily: 'Futura',
    fontSize: 16,
    fontWeight: 'bold'
  },
  detail: {
    margin: 8,
    fontSize: 15,
    fontFamily: 'Futura',
  },
});

class CampaignRow extends React.Component {

  state = {
    imgWidth: 0,
    imgHeight: 0,
  }

  setNativeProps(props: Object) {
    // super.setNativeProps(props);
  }

  componentDidMount() {

    Image.getSize(this.props.ImageUrl, (width, height) => {
      // calculate image width and height
      const screenWidth = Dimensions.get('window').width
      const scaleFactor = width / screenWidth
      const imageHeight = height / scaleFactor
      this.setState({imgWidth: screenWidth, imgHeight: imageHeight})
    })
  }

  render() {
    const {imgWidth, imgHeight} = this.state

    return (
      <View>
        <Image
          style={{width: imgWidth, height: imgHeight}}
          source={{uri: this.props.ImageUrl}}
        />
        <Text style={styles.title}>
          {this.props.Name}
        </Text>
        <Text style={styles.detail}>
          {this.props.Description}
        </Text>
      </View>
    )
  }
}

export default CampaignRow;
